library("MASS") 
library(formula.tools) 
data <- (Cars93)

proverka <- function(var1, var2, data) {
  if (is.null(data)) {
    warning("[ERROR] argument 'data' does not exist")
    return(NA_real_)
  }
  if (!is.data.frame(data)) {
    warning("[ERROR] argument 'data' is not a data frame")
    return(NA_real_)
  }

  if (!is.numeric(var1)) {
    warning("[ERROR] argument 'var1' is not a numerical value")
    return(NA_real_)
  }

  if (!is.numeric(var2)) {
    warning("[ERROR] argument 'var2' is not a numerical value")
    return(NA_real_)
  }
}

MyFunc <- function(formula, data = Cars93, FILENAME = "output.png") { 
  png(file = FILENAME, bg = "transparent") 

  target = lhs(formula) 
  target = as.character(target) 
  target = target[length(target)] 
  
  predictor = rhs(formula) 
  predictor = as.character(predictor) 
  predictor = predictor[length(predictor)] 
  
  proverka(data[[target]] ,data[[predictor]], data)

  model <- lm(formula) 
  plot(data[[predictor]], data[[target]], xlab = "�����", ylab = "������ ��������") 
  title("����������� ������� �������� �� �����") 
  abline(model) 
  segments(data[[predictor]], data[[target]], data[[predictor]], fitted(model), lty = 2, col = "red") 
  abline(h = median(data$Turn.circle)) 
  segments(data[[predictor]], data[[target]], data[[predictor]], median(data[[target]]), lty = 2, col = "blue") 
  
  dev.off() 
} 
MyFunc(data$Turn.circle ~ data$Length)